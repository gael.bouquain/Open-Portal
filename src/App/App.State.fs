namespace App

open Elmish
open Fable.Core
open Browser
open Feliz.Router

open API.Domoticz

open Pages


module Cmds =
    let loadDevices serverInfos =
        Cmd.ofSub (fun dispatch ->
            promise {
                let! result = API.Domoticz.Device.getDevices serverInfos

                result
                |> Msg.DevicesLoaded
                |> dispatch
            } |> Promise.start
        )

    let loadScenes serverInfos =
        Cmd.ofSub (fun dispatch ->
            promise {
                let! result = API.Domoticz.Scene.getScenes serverInfos

                result
                |> Msg.ScenesLoaded
                |> dispatch
            } |> Promise.start
        )


module Router =
    open App.Router

    let initFromRoute serverInfos errorMessageOpt route =
        match route, serverInfos with
        | Route.Login, _ ->
            let (pageModel, pageCmd) = Login.State.init errorMessageOpt
            pageModel |> Page.Login |> Some, pageCmd |> Cmd.map (PageMessage.Login >> Msg.PageMessage)

        | Route.Devices, Some serverInfos ->
            None, Cmds.loadDevices serverInfos

        | Route.Scenes, Some serverInfos ->
            None, Cmds.loadScenes serverInfos

        | Route.Home, _ -> None, Route.Scenes |> navigateToRouteCmd
        | Route.Devices, None
        | Route.Scenes, None
        | _ -> None, Route.Login |> navigateToRouteCmd


module State =
    let defaultModel =
        { Page = Page.Loading
          Loading = false
          ServerInfos = None
          ErrorMessage = None
          SettingsMenuOpened = None
          DrawerOpened = false }

    let updateHtmlTitle route =
        document.title <-
            match route with
            | Router.Route.Login -> "open portal | " + I18n.pages.login.title
            | Router.Route.Devices -> "open portal | " + I18n.pages.devices.title
            | Router.Route.Scenes -> "open portal | " + I18n.pages.scenes.title
            | _ -> "open portal"

            |> fun x -> x.ToLowerInvariant()

    let updatePageMessage serverInfosOpt model pageModel pageMessage =
        let setPage page =
            { model with Page = page }

        match pageMessage, pageModel, serverInfosOpt with
        | PageMessage.DeviceControl msg, Page.DeviceControl pageModel, Some serverInfos ->
            let newPageModel, pageCmd = DeviceControl.State.update serverInfos msg pageModel
            let cmd = pageCmd |> Cmd.map (PageMessage.DeviceControl >> Msg.PageMessage)
            newPageModel |> Page.DeviceControl |> setPage, cmd

        | PageMessage.Login msg, Page.Login pageModel, _ ->
            let newPageModel, pageCmd, outMsg = Login.State.update msg pageModel
            let cmd = pageCmd |> Cmd.map (PageMessage.Login >> Msg.PageMessage)

            match outMsg with
            | Some outMsg ->
                match outMsg with
                | Login.OutMsg.FormValidated (serverInfos, devices) ->
                    { model with
                        ServerInfos = serverInfos |> Some }, Router.Route.Scenes |> Router.navigateToRouteCmd

            | None ->
                Page.Login newPageModel |> setPage, cmd

        | PageMessage.Scenes msg, Page.Scenes pageModel, Some serverInfos ->
            let newPageModel, pageCmd = Scenes.State.update serverInfos msg pageModel
            let cmd = pageCmd |> Cmd.map (PageMessage.Scenes >> Msg.PageMessage)
            newPageModel |> Page.Scenes |> setPage, cmd

        | _ ->
            printfn "Invalid message"
            model, Cmd.none // invalid message


    let init () =
        let serverUrl = localStorage.getItem "serverUrl"
        let token = localStorage.getItem "token"

        let serverInfosExists =
            not (
                serverUrl |> JsInterop.isNullOrUndefined ||
                token |> JsInterop.isNullOrUndefined
            )

        match serverInfosExists with
        | false ->
            let (pageModel, pageCmd) = Login.State.init None

            Router.Route.Login |> updateHtmlTitle

            { defaultModel with Page = Page.Login pageModel },
            pageCmd |> Cmd.map (PageMessage.Login >> Msg.PageMessage)
        | true ->
            let serverInfos =
                { ServerAddress = serverUrl
                  Token = token }

            let route =
                Router.currentPath()
                |> Router.parseUrl

            let pageModelOpt, cmd =
                route |> Router.initFromRoute (Some serverInfos) None

            route |> updateHtmlTitle

            match pageModelOpt with
            | Some pageModel ->
                { defaultModel with
                    Page = pageModel
                    ServerInfos = Some serverInfos }, cmd
            | None ->
                { defaultModel with
                    ServerInfos = Some serverInfos }, cmd


    let update msg model =
        match msg with
        | Msg.PageMessage pageMsg ->
            updatePageMessage
                model.ServerInfos
                model
                model.Page
                pageMsg

        | Msg.RouteChanged route ->
            route
            |> Router.initFromRoute model.ServerInfos model.ErrorMessage
            |> fun (newPageOpt, cmd) ->
                route |> updateHtmlTitle

                match newPageOpt with
                | Some (Page.Login newPageModel) ->
                    { model with
                        Loading = false
                        Page = newPageModel |> Page.Login }, cmd
                | Some newPage ->
                    { model with Page = newPage }, cmd
                | None ->
                    model, cmd

        | Msg.ChangePage route ->
            { model with
                Loading = true
                DrawerOpened = false },
            route |> Router.navigateToRouteCmd

        | Msg.ScenesLoaded (Error error)
        | Msg.DevicesLoaded (Error error) ->
            let errorMessage =
                error
                |> Thoth.Fetch.errorToString

            { model with ErrorMessage = Some errorMessage },
            Router.Route.Login |> Router.navigateToRouteCmd

        | Msg.DevicesLoaded (Ok devices) ->
            let (newPageModel, pageCmd) = DeviceControl.State.init devices

            { model with
                Loading = false
                Page = newPageModel |> Page.DeviceControl },
            pageCmd |> Cmd.map (PageMessage.Login >> Msg.PageMessage)

        | Msg.ScenesLoaded (Ok scenes) ->
            let (newPageModel, pageCmd) = Scenes.State.init scenes

            { model with
                Loading = false
                Page = newPageModel |> Page.Scenes },
            pageCmd |> Cmd.map (PageMessage.Login >> Msg.PageMessage)

        | Msg.OpenDrawer -> { model with DrawerOpened = true }, Cmd.none
        | Msg.CloseDrawer -> { model with DrawerOpened = false }, Cmd.none

        | Msg.OpenSettingsMenu (x, y) ->
            { model with SettingsMenuOpened = (x, y) |> Some }, Cmd.none
        | Msg.CloseSettingsMenu -> { model with SettingsMenuOpened = None }, Cmd.none
        | Msg.MenuMsg menuMsg ->
            match menuMsg with
            | MenuMsg.Logout ->
                localStorage.clear()
                defaultModel, Router.Route.Login |> Router.navigateToRouteCmd