namespace Pages.Login

open Elmish
open Browser.Dom

open API.Domoticz

module Cmds =
    let getDevices form =
        Cmd.ofSub (fun dispatch ->
            promise {
                let token =
                    sprintf "%s:%s" form.Username form.Password
                    |> toBase64String
                    |> sprintf "Basic %s"

                let serverUrl = form.ServerAddress + ":" + form.ServerPort

                let! result =
                    Device.getDevices
                        { ServerAddress = serverUrl
                          Token = token }

                match result with
                | Error error ->
                    error
                    |> Msg.RequestError
                    |> dispatch
                | Ok result ->
                    window.localStorage.setItem
                        ("serverUrl", serverUrl)

                    window.localStorage.setItem
                        ("token", token)

                    result
                    |> Msg.RequestSucceed
                    |> dispatch
            } |> Promise.start
        )

module State =
    open Thoth.Fetch

    let init error =
        { Form =
            { ServerAddress = ""
              ServerPort = ""
              Username = ""
              Password = "" }
          Loading = false
          Error = error }, Cmd.none

    let update msg model : Model * Cmd<Msg> * OutMsg option =
        match msg with
        | Msg.FormChanged (formInput, operation) ->
            let newForm =
                match operation with
                | FormOperation.TextChanged newText ->
                    match formInput with
                    | FormInput.ServerAddress ->
                        { model.Form with ServerAddress = newText }
                    | FormInput.ServerPort ->
                        { model.Form with ServerPort = newText }
                    | FormInput.Username ->
                        { model.Form with Username = newText }
                    | FormInput.Password ->
                        { model.Form with Password = newText }
                | FormOperation.Clear ->
                    match formInput with
                    | FormInput.ServerAddress ->
                        { model.Form with ServerAddress = "" }
                    | FormInput.ServerPort ->
                        { model.Form with ServerPort = "" }
                    | FormInput.Username ->
                        { model.Form with Username = "" }
                    | FormInput.Password ->
                        { model.Form with Password = "" }

            { model with Form = newForm }, Cmd.none, None
        | Msg.ValidateForm ->
            { model with Loading = true },
            Cmds.getDevices model.Form,
            None

        | Msg.RequestError error ->
            let errorMessage = error |> errorToString

            { model with
                Loading = false
                Error = Some errorMessage },
                Cmd.none,
                None

        | Msg.RequestSucceed devices ->
            let token =
                (model.Form.Username, model.Form.Password)
                ||> sprintf "%s:%s"
                |> toBase64String
                |> sprintf "Basic %s"

            let serverInfos: ServerInfos =
                { ServerAddress = model.Form.ServerAddress + ":" + model.Form.ServerPort
                  Token = token }

            model, Cmd.none,
            (serverInfos, devices) |> OutMsg.FormValidated |> Some